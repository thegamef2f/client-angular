#-------------------------------------------------------------------------------------------------------------
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License. See https://go.microsoft.com/fwlink/?linkid=2090316 for license information.
#-------------------------------------------------------------------------------------------------------------

# To fully customize the contents of this image, use the following Dockerfile instead:
# https://github.com/microsoft/vscode-dev-containers/tree/v0.128.0/containers/typescript-node-14/.devcontainer/Dockerfile
FROM mcr.microsoft.com/vscode/devcontainers/typescript-node:0-14

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get -y install \
    #
    # Install basics
    vim \
    #
    # Install nginx
    nginx \
    #
    # Install angular cli
    && echo "n" | sudo npm install -g @angular/cli \
    #
    # Configure nginx
    && rm -rf /etc/nginx/conf.d \
    && mkdir /etc/nginx/conf.d \
    && ln -fs /workspaces/client-angular/nginx/conf.d/default.dev.conf /etc/nginx/conf.d/default.conf \
    #
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
