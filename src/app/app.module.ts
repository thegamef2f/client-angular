import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlayerComponent } from './player/player.component';
import { OpponentComponent } from './opponent/opponent.component';
import { DrawPileComponent } from './draw-pile/draw-pile.component';
import { HandCardsComponent } from './hand-cards/hand-cards.component';
import { PilesComponent } from './piles/piles.component';
import { CardComponent } from './card/card.component';
import { ArrayPipe } from './array.pipe';
import { appReducer } from './app.reducer';

@NgModule({
  declarations: [
    AppComponent,
    PlayerComponent,
    OpponentComponent,
    DrawPileComponent,
    HandCardsComponent,
    PilesComponent,
    CardComponent,
    ArrayPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot({ app: appReducer }),
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
