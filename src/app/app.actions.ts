import { createAction, props } from '@ngrx/store';

import { Status } from './status';

export const setPlayerId = createAction('[App] Set PlayerId', props<{sessionId: string}>());
export const selectCard = createAction('[App] Select Card', props<{card: number}>());
export const updateStatus = createAction('[App] Update Status', props<Status>());
