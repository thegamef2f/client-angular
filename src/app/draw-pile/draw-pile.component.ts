import { Component, Input, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { Side } from '../side';
import { AppService } from '../app.service';
import { Status } from '../status';

@Component({
  selector: 'app-draw-pile',
  templateUrl: './draw-pile.component.html',
  styleUrls: ['./draw-pile.component.css']
})
export class DrawPileComponent implements OnInit {
  @Input() side: Side;
  public status: Status;

  constructor(private store: Store<{ app: Status }>, private appService: AppService) {}

  ngOnInit(): void {
    this.store.pipe(select('app')).subscribe((status: Status) => {
      this.status = status;
    });
  }

  public isClickable(): boolean {
    return this.side === Side.player && this.status.active;
  }

  public drawCards(): void {
    if (!this.status.active) {
      return;
    }
    this.appService.drawCards();
  }
}
