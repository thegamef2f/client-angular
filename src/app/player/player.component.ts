import { Component, OnInit } from '@angular/core';

import { Store, select } from '@ngrx/store';

import { Status } from '../status';
import { selectCard } from '../app.actions';
import { AppService } from '../app.service';
import { Side } from '../side';

interface Play { card: number; side: any; pile: any; }

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  public status: Status;
  public Side = Side;

  constructor(private store: Store<{ app: Status }>, private appService: AppService) {}

  ngOnInit(): void {
    this.store.pipe(select('app')).subscribe((status: Status) => {
      this.status = status;
    });
  }

  public selectCard(card: number): void {
    if (!this.status.active) {
      return;
    }
    if (this.status.selectedCard && this.status.selectedCard === card) {
      this.store.dispatch(selectCard(null));
      return;
    }
    this.store.dispatch(selectCard({card}));
  }
}
