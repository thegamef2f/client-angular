import { Action, createReducer, on } from '@ngrx/store';
import { selectCard, setPlayerId, updateStatus } from './app.actions';
import { Status } from './status';

export const initialState: Status = {
  sessionId: null,
  active: false,
  opponentColor: null,
  playerColor: null,
  opponentName: null,
  playerName: null,
  playerCards: [],
  pilesPlayer: { up: 1, down: 60 },
  numOpponentCards: 0,
  pilesOpponent: { up: 1, down: 60 },
  selectedCard: null,
  winnerColor: null,
};

const reducer = createReducer(
  initialState,
  on(setPlayerId, (status, { sessionId }) => ({ ...status, sessionId })),
  on(selectCard, (status, { card }) => ({ ...status, selectedCard: card })),
  on(updateStatus, (status, newStatus) => ({
    ...newStatus,
    selectedCard: status.selectedCard,
    sessionId: status.sessionId,
  }))
);

export function appReducer(state: Status, action: Action): Status {
  return reducer(state, action);
}
