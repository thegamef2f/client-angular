import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { Status } from '../status';
import { AppService } from '../app.service';

@Component({
  selector: 'app-hand-cards',
  templateUrl: './hand-cards.component.html',
  styleUrls: ['./hand-cards.component.css']
})
export class HandCardsComponent implements OnInit {
  @Input() cards: number[];
  @Input() numCards: number;
  @Input() selectedCard: number;
  @Output() selectCard = new EventEmitter<void>();
  public status: Status;

  constructor(private store: Store<{ app: Status }>, private appService: AppService) {}

  ngOnInit(): void {
    this.store.pipe(select('app')).subscribe((status: Status) => {
      this.status = status;
    });
  }
}
