import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() card: number;
  @Input() selected: boolean;

  public url: string;

  ngOnInit(): void {
    if (this.card) {
      this.url = `assets/card_gold_${this.card}.svg`;
    } else {
      this.url = 'assets/card_gold.svg';
    }
  }
}
