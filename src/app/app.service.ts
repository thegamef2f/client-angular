import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Store, select } from '@ngrx/store';

import { Status } from './status';
import { setPlayerId, updateStatus, selectCard } from './app.actions';
import { Side } from './side';

function sideToString(side: Side): string {
  switch (side) {
    case Side.player: return 'player';
    case Side.opponent: return 'opponent';
    default: return '';
  }
}

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private status: Status;
  private url = '/api/v1/game';

  constructor(private http: HttpClient, private store: Store<{ app: Status }>) {
    this.store.pipe(select('app')).subscribe((status: Status) => {
      this.status = status;
    });
    this.getStatus();
    setInterval(() => {
      this.getStatus();
    }, 2000);
  }

  public setSessionId(sessionId: string): void {
    this.store.dispatch(setPlayerId({ sessionId }));
    this.getStatus();
  }

  public playCard(side: Side, pile: string): void {
    this.http.get(`${this.url}/playCard`, { params: { sessionId: this.status.sessionId,
      card: this.status.selectedCard.toString(), side: sideToString(side), pile }}).subscribe(() => {
        this.getStatus();
    });
    this.store.dispatch(selectCard(null));
  }

  public drawCards(): void {
    this.http.get(`${this.url}/drawCards`, { params: { sessionId: this.status.sessionId }}).subscribe(() => {
      this.getStatus();
    });
  }

  private getStatus(): void {
    if (!this.status.sessionId) {
      return;
    }
    this.http.get(`${this.url}/getStatus`, { params: { sessionId: this.status.sessionId }}).subscribe((status: Status) => {
      this.store.dispatch(updateStatus(status));
    });
  } 
}
