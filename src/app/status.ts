export interface Status {
  active: boolean;
  numOpponentCards: number;
  opponentColor: string;
  opponentName: string;
  pilesOpponent: { down: number; up: number };
  pilesPlayer: { down: number; up: number };
  playerCards: number[];
  playerColor: string;
  playerName: string;
  selectedCard: number;
  sessionId: string;
  winnerColor: string;
}
