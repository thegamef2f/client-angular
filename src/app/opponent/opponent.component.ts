import { Component, Input, OnInit } from '@angular/core';

import { Store, select } from '@ngrx/store';

import { Status } from '../status';
import { Side } from '../side';

@Component({
  selector: 'app-opponent',
  templateUrl: './opponent.component.html',
  styleUrls: ['./opponent.component.css']
})
export class OpponentComponent implements OnInit {
  @Input() name: string;
  @Input() active: boolean;
  @Input() numCards: number;
  @Input() piles: {up: number, down: number};
  public status: Status;
  public Side = Side;

  constructor(private store: Store<{ app: Status }>) {}

  ngOnInit(): void {
    this.store.pipe(select('app')).subscribe((status: Status) => {
      this.status = status;
    });
  }
}
