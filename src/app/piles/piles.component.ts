import { Component, Input, OnInit } from '@angular/core';

import { Store, select } from '@ngrx/store';

import { Status } from '../status';
import { AppService } from '../app.service';
import { Side } from '../side';

@Component({
  selector: 'app-piles',
  templateUrl: './piles.component.html',
  styleUrls: ['./piles.component.css']
})
export class PilesComponent implements OnInit {
  @Input() side: Side;
  public status: Status;
  public up: number;
  public down: number;

  constructor(private store: Store<{ app: Status }>, private appService: AppService) {}

  ngOnInit(): void {
    this.store.pipe(select('app')).subscribe((status: Status) => {
      this.status = status;
      if (this.side === Side.player) {
        this.up = this.status.pilesPlayer.up === 1 ? null : this.status.pilesPlayer.up;
        this.down = this.status.pilesPlayer.down === 60 ? null : this.status.pilesPlayer.down;
      } else if (this.side === Side.opponent) {
        this.up = this.status.pilesOpponent.up === 1 ? null : this.status.pilesOpponent.up;
        this.down = this.status.pilesOpponent.down === 60 ? null : this.status.pilesOpponent.down;
      }
    });
  }

  public playCard(pile: string): void {
    if (!this.status.active) {
      return;
    }
    if (!this.status.selectedCard) {
      return;
    }
    this.appService.playCard(this.side, pile);
  }
}
