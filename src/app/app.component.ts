import { Component, OnInit } from '@angular/core';

import { Store, select } from '@ngrx/store';

import { Status } from './status';
import { AppService } from './app.service';

interface Play {
  card: number;
  side: any;
  pile: any;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public status: Status;
  public message: string | null = null;

  constructor(
    private store: Store<{ app: Status }>,
    private appService: AppService
  ) {}

  ngOnInit(): void {
    const sessionId = window.location.search.split('?').pop().split('=').pop();
    this.appService.setSessionId(sessionId);
    this.store.pipe(select('app')).subscribe((status: Status) => {
      this.status = status;
      if (this.status.winnerColor) {
        if (this.status.winnerColor === this.status.playerColor) {
          this.message = 'Gewonnen';
        } else {
          this.message = 'Verloren';
        }
      } else {
        this.message = null;
      }
    });
  }
}
