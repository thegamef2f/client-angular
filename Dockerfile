FROM node:14.5.0 as builder

COPY . /workspace
WORKDIR /workspace
RUN npm i && npm run build

FROM nginx:1.19.1

COPY ./nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /workspace/dist /usr/share/nginx/html
